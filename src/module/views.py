import uuid

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect

from module.models import Person


def main(request):
    print(request.user)
    if not request.user.is_authenticated:
        return redirect('login')

    return redirect('admin')


def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            if user.totp is None:
                login(request, user)
                return redirect('events_list')

            else:
                request.session['uuid'] = user.uuid.__str__()
                return redirect('drk_login_second')
        else:
            return render(request, 'drk_core/login.html', {'username': username, 'lg_error': True})
    else:
        return render(request, 'drk_core/login.html')


def login_second_view(request):
    if request.method == 'POST':
        code = request.POST['code']
        user = None
        try:
            user = Person.objects.get(uuid=uuid.UUID(request.session['uuid'].__str__()))
            print(user.username)
        except:
            print("UE")

        if user is not None:
            totp = pyotp.TOTP(user.totp)
            if totp.verify(code):
                login(request, user)
                request.session['uuid'] = None
                return redirect('events_list')
            else:
                return render(request, 'drk_core/loginSecond.html', {'lg_error': True})
        else:
            return redirect('drk_login')
    else:

        return render(request, 'drk_core/loginSecond.html')


def logout_view(request):
    if not request.user.is_authenticated:
        return redirect('drk_login')

    logout(request)
    return redirect('drk_login')
