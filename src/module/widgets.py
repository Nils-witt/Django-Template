import sys
from datetime import datetime

from django.forms import DateTimeInput


class BootstrapDateTimePickerInput(DateTimeInput):
    template_name = 'widgets/bootstrap_datetimepicker.html'

    def get_context(self, name, value, attrs):
        datetimepicker_id = 'datetimepicker_{name}'.format(name=name)
        revalue = value
        try:
            revalue = value.strftime("%d.%m.%Y %H:%M")
        except:
            print(sys.exc_info()[0])

        if attrs is None:
            attrs = dict()
        attrs['data-target'] = '#{id}'.format(id=datetimepicker_id)
        attrs['class'] = 'form-control datetimepicker-input'
        context = super().get_context(name, revalue, attrs)
        context['widget']['datetimepicker_id'] = datetimepicker_id
        return context

    def value_from_datadict(self, data, files, name):
        return datetime.strptime(data.get(name, None), '%d.%m.%Y %H:%M')
