from django.contrib import admin

from module.models import Person


class PersonAdmin(admin.ModelAdmin):
    list_per_page = 2000
    list_display = ['username', 'last_name', 'first_name']

    # ordering = ['last_name', 'first_name']
    # ordering = ['times_deployed']


admin.site.register(Person, PersonAdmin)
