import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models


class Person(AbstractUser):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    username = models.CharField(max_length=100, unique=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return self.last_name + ", " + self.first_name
